/* eslint-disable prettier/prettier */
import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {RNCamera} from 'react-native-camera';
import BarcodeMask from 'react-native-barcode-mask';

export default class Scanner extends Component {
  componentDidMount() {
    const screen = Dimensions.get('screen');
    console.log('screen width: ', screen.width);
    console.log('screen height: ', screen.height);
    const window = Dimensions.get('window');
    console.log('window width: ', window.width);
    console.log('window height: ', window.height);
  }

  getWidth = () => {
    const screen = Dimensions.get('screen');
    return screen.width;
  };

  getHeight = () => {
    const screen = Dimensions.get('screen');
    let width = screen.width;
    return width / 1.5;
  };

  scannPressed = async () => {
    if (this.camera) {
      const options = {quality: 0.6};
      const data = await this.camera.takePictureAsync({
        quality: 0.5,
        orientation: 'portrait',
        //width: 1200,
      });
      if (this.props.onScannCard) {
        this.props.onScannCard(data);
      }
      this.props.onBack();
    }
  };

  render() {
    return (
      <View style={styles.rootContainer}>
        <RNCamera
          ref={(ref) => {
            this.camera = ref;
          }}
          style={styles.preview}>
          <BarcodeMask
            width={this.getWidth()}
            height={this.getHeight()}
            edgeColor={'#62B1F6'}
            showAnimatedLine={false}
          />
          <TouchableOpacity
            style={styles.buttonContainer}
            onPress={this.scannPressed}>
            <View style={styles.button}>
              <Text style={styles.font16}>Scann</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.buttonContainer}
            onPress={this.props.onBack}>
            <View style={styles.button}>
              <Text style={styles.font16}>Back</Text>
            </View>
          </TouchableOpacity>
        </RNCamera>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  rootContainer: {
    flex: 1,
  },
  button: {
    width: 200,
    height: 40,
    borderWidth: 1,
    borderRadius: 6,
    backgroundColor: '#eee',
    justifyContent: 'center',
    alignItems: 'center',
  },
  font16: {fontSize: 16},
  buttonContainer: {
    marginVertical: 10,
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
});

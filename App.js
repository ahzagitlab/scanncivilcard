/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  Image,
  Dimensions,
} from 'react-native';
import ImageEditor from '@react-native-community/image-editor';
import Scanner from './src/Components/Scanner';
import image from './src/Assets/scann.png';
import {utils} from '@react-native-firebase/app';
import vision from '@react-native-firebase/ml-vision';

class App extends Component {
  state = {
    scannCard: false,
    imageUri: '',
  };

  scannCard = () => {
    this.setState({scannCard: true});
  };

  back = () => {
    this.setState({scannCard: false});
  };

  onScannCard = (data) => {
    console.log('data', data);
    const screen = Dimensions.get('window');
    const picHeight = data.height;
    const picWidth = data.width;
    const ratio = picWidth / screen.height;
    const cardHeight = screen.width * ratio;
    const cardWidth = cardHeight / 1.5;
    const offsetX = (picWidth - cardWidth) / 2;
    const offsetY = (picHeight - cardHeight) / 2;

    const cropData = {
      offset: {x: offsetX, y: offsetY},
      size: {width: cardWidth, height: cardHeight},
      displaySize: {width: 1600, height: 2400},
    };
    ImageEditor.cropImage(data.uri, cropData).then(
      (url) => {
        console.log('url', url);
        this.setState({imageUri: url});
        this.processDocument(url);
      },
      (error) => {
        console.log('error', error);
      },
    );
  };

  processDocument = async (localPath) => {
    const processed = await vision().textRecognizerProcessImage(localPath);

    console.log('Found text in document: ', processed.text);
    console.log('processed.blocks', processed.blocks);
    processed.blocks.forEach((block) => {
      console.log('Found block with text: ', block.text);
      // console.log('Confidence in block: ', block.confidence);
      // console.log('Languages found in block: ', block.recognizedLanguages);
    });
  };

  render() {
    let source = image;
    if (this.state.imageUri) {
      source = {uri: this.state.imageUri};
    }

    return this.state.scannCard ? (
      <Scanner onBack={this.back} onScannCard={this.onScannCard} />
    ) : (
      <View style={styles.rootContainer}>
        <Image source={source} style={styles.image} />
        <TouchableOpacity style={styles.mt20} onPress={this.scannCard}>
          <View style={styles.button}>
            <Text style={styles.font16}>Scann Card</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  rootContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    width: 200,
    height: 40,
    borderWidth: 1,
    borderRadius: 6,
    backgroundColor: '#eee',
    justifyContent: 'center',
    alignItems: 'center',
  },
  font16: {fontSize: 16},
  mt20: {marginTop: 20},
  image: {width: 400, height: 266.67, resizeMode: 'stretch'},
});
export default App;
